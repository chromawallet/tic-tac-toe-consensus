import React, {Component} from 'react';
import TextField from "@material-ui/core/TextField";
import PropTypes from 'prop-types';
import {InputLabel, withStyles} from "@material-ui/core";

import classNames from 'classnames';
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import Button from "@material-ui/core/Button";
import {gameOperations} from "../../state/ducks/game";
import {accountOperations, accountSelectors} from "../../state/ducks/account";
import connect from "react-redux/es/connect/connect";
import {getPlayer, registerPlayer} from "../../state/utils/blockchain";
import MySnackbarContent from "./MySnackbarContent";


const styles = theme => ({
    root: {
        width: 500,
    },
    container: {
        display: 'inline-grid',
        width: '100%',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
    formControl: {
        margin: theme.spacing.unit,
    },
});

class Login extends Component {
    constructor(props) {
        super(props);
        this.state ={ isError: null};

        // this.props.setName("Riccardo");
        // this.props.generatePubKey("f7372e8246787cb8cee1dd4223aa7421a21e393afc6127e919a8d7a9d07d8af1");
        // this.props.login();
    }

    // handleChange = name => event => {
    handleChange = (event, name) => {
        switch (name) {
            case "name":
                this.props.setName(event.target.value);
                break;
            case "privKey":
                this.props.generatePubKey(event.target.value);
                break;
        }
    };

    onClickGenerateKeyPair = () => {
        this.props.generateKeyPair();
    };

    setError = (error) => {
        this.setState({isError: error});
        window.setTimeout(() => {
            this.setState({isError: null});
        }, 3000);
    };

    onLogin = async () => {
        const {name, pubKey, privKey} = this.props.account;
        const userExist = await getPlayer(name, pubKey);
        if(!userExist) {
            try{
                await registerPlayer({pubKey, privKey}, name);
            } catch(e) {
                const errorMessage = "Something went wrong... Maybe name already taken?";
                this.setError(errorMessage);
                throw Error(e);
            }
        }
        this.props.login();
    }

    render() {

        const classes = this.props.classes;
        return <div className={classes.root}>
            <Typography component="h2" variant="h1" gutterBottom>
                Login
            </Typography>
            <form className={classes.container} noValidate autoComplete="off">
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="component-helper">Name</InputLabel>
                    <Input
                        id="name"
                        value={this.props.account.name}
                        onChange={(event) => this.handleChange(event, "name")}
                        aria-describedby="component-helper-text"
                    />
                </FormControl>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="component-helper">Private key</InputLabel>
                    <Input
                        multiline={true}
                        rows={2}
                        id="component-helper"
                        value={this.props.account.privKey}
                        onChange={(event) => this.handleChange(event, 'privKey')}
                        aria-describedby="component-helper-text"
                    />
                </FormControl>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="component-helper">Public Key</InputLabel>
                    <Input disabled
                        multiline={true}
                        rows={2}
                        id="component-helper"
                        value={this.props.account.pubKey}
                        onChange={(event) => this.handleChange(event, 'privKey')}
                        aria-describedby="component-helper-text"
                    />
                </FormControl>
                <Button variant="contained" className={classes.button} onClick={this.onClickGenerateKeyPair}>
                    Generate Keypair
                </Button>
                or
                <Button variant="contained" color="primary" className={classes.button} onClick={this.onLogin}>
                    Log in
                </Button>
                {this.state.isError && <MySnackbarContent
                    variant="error"
                    className={classes.margin}
                    message={this.state.isError}
                />}
            </form>
        </div>
    }
}


const {func, object} = PropTypes;
Login.propTypes = {
    classes: PropTypes.object.isRequired,
    setName: func.isRequired,
    generatePubKey: func.isRequired,
    generateKeyPair: func.isRequired,
    login: func.isRequired,
};

const mapStateToProps = (state) => {
    return {
        ...state,
        account: accountSelectors.getAccount(state),
    };
};

const mapDispatchToProps = {
    setName: accountOperations.settingName,
    generatePubKey: accountOperations.generatePub,
    generateKeyPair: accountOperations.generateKeyPair,
    login: accountOperations.login
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Login));
