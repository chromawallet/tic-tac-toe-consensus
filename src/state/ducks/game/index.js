import reducer from './reducers';

import * as gameOperations from './operations';
import * as gameSelectors from './selector';
import * as gameTypes from './types';

export {
    gameOperations,
    gameSelectors,
    gameTypes,
};

export default reducer;
