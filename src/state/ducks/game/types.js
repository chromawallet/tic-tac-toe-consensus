
const NEW_GAME = 'GAME_NEW';
const GAMEOVER = 'GAME_OVER';
const MOVE = 'GAME_MOVE';
const PLAYER = 'GAME_PLAYER';
const WINNER = 'GAME_WINNER';

const SET_BOARD = "SET_BOARD";
const SET_SIGN = "SET_SIGN";

export {
    NEW_GAME,
    GAMEOVER,
    MOVE,
    PLAYER,
    WINNER,

    SET_BOARD,
    SET_SIGN
}