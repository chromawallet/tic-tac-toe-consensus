import { put, takeEvery, select } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import {getOpenTables, getPlayerTables, newGame, getGames } from "../../utils/blockchain";
import { addPlayer } from "../../utils/blockchain";
import {setOpenTables} from "./actions";

function* loadPlayerTables() {
    const pubKey = yield select(state => state.accountState.account.pubKey);
    const playerTables = yield getPlayerTables(pubKey);
    yield put({type: "ADD_PLAYER_TABLES", payload: playerTables})
}

function* joinGame(action) {
    console.log('-------------- joining game inside saga -------------');
    const account = yield select(state => state.accountState.account);
    yield addPlayer(account, action.payload, account.name);
    yield put(push(`/games/${action.payload}`));
}

function* openGame(action) {
    yield put(push(`/games/${action.payload}`));
}

function* loadOpenTables() {
    const pubKey = yield select(state => state.accountState.account.pubKey);
    const openTables = yield getOpenTables(pubKey);
    yield put(setOpenTables(openTables));
}

function* createGame() {
    const account = yield select(state => state.accountState.account);
    yield newGame(account, Date.now().toString(), account.name);
    yield put(push('/games/my'));
}

export default function* tableSaga(store) {
    yield takeEvery("LOAD_OPEN_TABLES", loadOpenTables);
    yield takeEvery("LOAD_PLAYER_TABLES", loadPlayerTables);
    yield takeEvery("JOIN_GAME", joinGame);
    yield takeEvery("CREATE_GAME", createGame);
    yield takeEvery("OPEN_GAME", openGame);
}
