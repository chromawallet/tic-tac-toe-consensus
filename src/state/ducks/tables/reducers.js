import {combineReducers} from 'redux';

import * as types from './types';

const tableReducer = (state = {}, action) => {
    switch (action.type) {
        case types.ADD_PLAYER_TABLES:
            return {...state, playerTables: action.payload};
        case types.ADD_OPEN_TABLES:
            return {...state, openTables: action.payload};
        default:
            return state
    }
};

export default combineReducers({
    tables: tableReducer,
});